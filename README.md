# Adaptive Navigation Challenge

Flutter challenge with navigation adaptive to the screen size.

## Features

- The app is created as an adaptive navigation showcase.
- The app displays a bottom navigation bar on small screens, and a navigation rail on big screens.
- A list with tappable elements is shown on the first page, displaying a detail page on the next screen on small screens, and aside on big screens.

## Releases

See this project working on web at this link:

[WebApp](https://cbau.gitlab.io/adaptive_navigation_challenge/)

## Compile

To run this project on your device, run this command:

```bash
flutter run --release
```
