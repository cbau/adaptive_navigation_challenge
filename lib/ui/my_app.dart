import 'package:flutter/material.dart';

import 'app_router.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerDelegate: AppRouter().routerDelegate,
      routeInformationParser: AppRouter().routeInformationParser,
      routeInformationProvider: AppRouter().routeInformationProvider,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      title: 'Adaptive Navigation Challenge',
    );
  }
}
