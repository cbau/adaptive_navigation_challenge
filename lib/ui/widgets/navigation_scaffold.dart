import 'package:flutter/material.dart';

class NavigationScaffold extends StatelessWidget {
  final PreferredSizeWidget? appBar;
  final Widget? body;
  final List<NavigationItem> items;
  final int selectedIndex;
  final Function(int)? onDestinationSelected;

  const NavigationScaffold({
    required this.items,
    this.appBar,
    this.body,
    this.onDestinationSelected,
    this.selectedIndex = 0,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constrains) => Scaffold(
        appBar: appBar,
        body: constrains.maxWidth < 600
            ? body
            : Row(
                children: [
                  NavigationRail(
                    destinations: [
                      ...items.map(
                        (e) => NavigationRailDestination(
                          icon: e.icon,
                          label: Text(e.label ?? ''),
                        ),
                      ),
                    ],
                    labelType: NavigationRailLabelType.all,
                    onDestinationSelected: onDestinationSelected,
                    selectedIndex: selectedIndex,
                  ),
                  Expanded(
                    child: body ?? const SizedBox(),
                  ),
                ],
              ),
        bottomNavigationBar: constrains.maxWidth < 600
            ? BottomNavigationBar(
                currentIndex: selectedIndex,
                onTap: onDestinationSelected,
                type: BottomNavigationBarType.fixed,
                items: [
                  ...items.map(
                    (e) => BottomNavigationBarItem(
                      icon: e.icon,
                      label: e.label,
                    ),
                  ),
                ],
              )
            : null,
      ),
    );
  }
}

class NavigationItem {
  final Widget icon;
  final String? label;

  const NavigationItem({
    required this.icon,
    this.label,
  });
}
