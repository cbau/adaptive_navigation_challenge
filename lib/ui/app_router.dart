import 'package:adaptive_navigation_challenge/ui/app_colors.dart';
import 'package:adaptive_navigation_challenge/ui/pages/navigation_page.dart';
import 'package:adaptive_navigation_challenge/ui/pages/not_found_page.dart';
import 'package:adaptive_navigation_challenge/ui/pages/placeholder_details_page.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class AppRouter {
  static final AppRouter _instance = AppRouter._();

  AppRouter._();

  factory AppRouter() => _instance;

  final _router = GoRouter(
    errorBuilder: (context, state) => const NotFoundPage(),
    routes: [
      GoRoute(
        path: '/',
        redirect: (state) => '/news',
      ),
      GoRoute(
        pageBuilder: (context, state) => CustomTransitionPage(
          transitionsBuilder: (context, animation, secondaryAnimation, child) =>
              FadeTransition(
            opacity: animation,
            child: child,
          ),
          child: const NavigationPage(
            currentIndex: 0,
          ),
        ),
        path: '/news',
      ),
      GoRoute(
        builder: (context, state) {
          final id = int.tryParse(state.params['id'] ?? '') ?? 1;
          return Scaffold(
            appBar: AppBar(
              title: Text('Item #$id'),
            ),
            body: PlaceholderDetailsPage(
              backgroundColor: AppColors.list[(id - 1) % AppColors.list.length],
            ),
          );
        },
        path: '/news/:id',
      ),
      GoRoute(
        pageBuilder: (context, state) => CustomTransitionPage(
          transitionsBuilder: (context, animation, secondaryAnimation, child) =>
              FadeTransition(
            opacity: animation,
            child: child,
          ),
          child: const NavigationPage(
            currentIndex: 1,
          ),
        ),
        path: '/about',
      ),
      GoRoute(
        pageBuilder: (context, state) => CustomTransitionPage(
          transitionsBuilder: (context, animation, secondaryAnimation, child) =>
              FadeTransition(
            opacity: animation,
            child: child,
          ),
          child: const NavigationPage(
            currentIndex: 2,
          ),
        ),
        path: '/map',
      ),
      GoRoute(
        pageBuilder: (context, state) => CustomTransitionPage(
          transitionsBuilder: (context, animation, secondaryAnimation, child) =>
              FadeTransition(
            opacity: animation,
            child: child,
          ),
          child: const NavigationPage(
            currentIndex: 3,
          ),
        ),
        path: '/events',
      ),
      GoRoute(
        pageBuilder: (context, state) => CustomTransitionPage(
          transitionsBuilder: (context, animation, secondaryAnimation, child) =>
              FadeTransition(
            opacity: animation,
            child: child,
          ),
          child: const NavigationPage(
            currentIndex: 4,
          ),
        ),
        path: '/profile',
      ),
    ],
  );

  RouterDelegate<Object> get routerDelegate => _router.routerDelegate;

  RouteInformationParser<Object> get routeInformationParser =>
      _router.routeInformationParser;

  RouteInformationProvider get routeInformationProvider =>
      _router.routeInformationProvider;
}
