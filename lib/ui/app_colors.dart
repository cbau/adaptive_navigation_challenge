import 'package:flutter/painting.dart';

class AppColors {
  static const blue = Color(0xFF9999FF);
  static const cyan = Color(0xFF99FFFF);
  static const green = Color(0xFF99FF99);
  static const magenta = Color(0xFFFF99FF);
  static const red = Color(0xFFFF9999);
  static const yellow = Color(0xFFFFFF99);

  static const list = [
    red,
    yellow,
    green,
    cyan,
    blue,
    magenta,
  ];
}
