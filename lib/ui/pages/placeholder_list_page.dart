import 'package:adaptive_navigation_challenge/ui/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class PlaceholderListPage extends StatelessWidget {
  const PlaceholderListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 50,
      itemBuilder: (context, index) => ListTile(
        onTap: () => GoRouter.of(context).go('/news/${index + 1}'),
        tileColor: AppColors.list[index % AppColors.list.length],
        title: Text('Item #${index + 1}'),
      ),
    );
  }
}
