import 'package:adaptive_navigation_challenge/ui/app_colors.dart';
import 'package:adaptive_navigation_challenge/ui/pages/placeholder_list_page.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../widgets/navigation_scaffold.dart';
import 'placeholder_details_page.dart';

class NavigationPage extends StatefulWidget {
  static const String routeName = '/';

  final int currentIndex;

  const NavigationPage({
    required this.currentIndex,
    Key? key,
  }) : super(key: key);

  @override
  State<NavigationPage> createState() => _NavigationPageState();
}

class _NavigationPageState extends State<NavigationPage> {
  final _items = const [
    NavigationItem(
      icon: Icon(Icons.newspaper),
      label: 'News',
    ),
    NavigationItem(
      icon: Icon(Icons.info),
      label: 'About',
    ),
    NavigationItem(
      icon: Icon(Icons.map),
      label: 'Map',
    ),
    NavigationItem(
      icon: Icon(Icons.event),
      label: 'Events',
    ),
    NavigationItem(
      icon: Icon(Icons.person),
      label: 'Profile',
    ),
  ];

  final _paths = const [
    '/news',
    '/about',
    '/map',
    '/events',
    '/profile',
  ];

  final _widgetOptions = const [
    PlaceholderListPage(),
    PlaceholderDetailsPage(
      backgroundColor: AppColors.yellow,
    ),
    PlaceholderDetailsPage(
      backgroundColor: AppColors.green,
    ),
    PlaceholderDetailsPage(
      backgroundColor: AppColors.cyan,
    ),
    PlaceholderDetailsPage(
      backgroundColor: AppColors.blue,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return NavigationScaffold(
      appBar: AppBar(
        title: Text(_items[widget.currentIndex].label!),
      ),
      body: _widgetOptions.elementAt(widget.currentIndex),
      selectedIndex: widget.currentIndex,
      onDestinationSelected: (index) => GoRouter.of(context).go(_paths[index]),
      items: _items,
    );
  }
}
