import 'package:flutter/material.dart';

class PlaceholderDetailsPage extends StatelessWidget {
  final Color backgroundColor;
  final Widget? child;

  const PlaceholderDetailsPage({
    required this.backgroundColor,
    this.child,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundColor,
      child: child,
    );
  }
}
